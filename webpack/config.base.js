/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

require('dotenv').config()

const webpack = require('webpack')
const path = require('path')

module.exports = {
  output: {
    filename: 'js/app-[hash].js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/'
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: [
          'babel-loader'
        ],
        exclude: /node_modules/
      },
      {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: 'graphql-tag/loader'
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/,
        use: [
          {
            loader: 'file-loader?name=img/[name]-[hash].[ext]'
          }
        ]
      }
    ]
  },

  plugins: [
    new webpack.EnvironmentPlugin([
      'GRAPHQL_URI'
    ])
  ],

  resolve: {
    extensions: ['.js', '.json', '.gql'],
    modules: [
      'node_modules',
      path.join(__dirname, '../src')
    ]
  }
}
