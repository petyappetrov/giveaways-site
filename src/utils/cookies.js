/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import { find } from 'lodash'

const cookies = {
  get: (name, cookies) => {
    const list = cookies
      ? cookies.split(';')
      : document.cookie.split(';')

    let result = null

    find(list, item => {
      const parts = item.split('=')
      if (parts[0].trim() === name) {
        result = parts[1].trim()
        return true
      }
      return false
    })

    return result
  },

  set: (name, value) => {
    document.cookie = name + '=' + value + '; path=/'
  },

  remove: (name) => {
    document.cookie = name + '=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;'
  }
}

export default cookies
