/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import MobileDetect from 'mobile-detect'

export default userAgent => {
  const md = new MobileDetect(userAgent)

  if (md.mobile()) {
    return {
      fallbackWidth: 360,
      fallbackHeight: 640
    }
  } else if (md.tablet()) {
    return {
      fallbackWidth: 768,
      fallbackHeight: 1024
    }
  }

  return {
    fallbackWidth: 1280,
    fallbackHeight: 700
  }
}
