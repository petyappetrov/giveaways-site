/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

export default {
  newest: {
    sortField: 'createdAt',
    sortOrder: 'descend'
  },
  ending: {
    sortField: 'endDate',
    sortOrder: 'ascend'
  }
}
