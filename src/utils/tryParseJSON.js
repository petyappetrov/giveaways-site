/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

export default (json) => {
  try {
    return JSON.parse(json)
  } catch (e) {
    return {}
  }
}
