/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import queryString from 'query-string'
import { isNil, omitBy } from 'lodash'

export default (WrappedComponent) => {
  class HandleQueries extends React.Component {
    updateQuery = (values) => {
      const { location, history } = this.props
      const query = queryString.parse(location.search)
      history.push({
        search: queryString.stringify(omitBy({ ...query, ...values }, isNil))
      })
    }
    render () {
      return (
        <WrappedComponent
          {...this.props}
          query={queryString.parse(this.props.location.search)}
          updateQuery={this.updateQuery}
        />
      )
    }
  }

  HandleQueries.propTypes = {
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  }

  return HandleQueries
}
