/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'

const Html = ({ content, styleTags, bundleSrc, client: { cache } }) =>
  <html lang='ru'>
    <head>
      <meta charSet='utf-8' />
      <meta name='viewport' content='width=device-width, initial-scale=1' />
      <link href='https://use.fontawesome.com/releases/v5.2.0/css/svg-with-js.css' rel='stylesheet' />
      {styleTags}
    </head>
    <body>
      <div id='root' dangerouslySetInnerHTML={{ __html: content }} />
      <script
        charSet='UTF-8'
        dangerouslySetInnerHTML={{
          __html: `window.__APOLLO_STATE__=${JSON.stringify(cache.extract())};`
        }}
      />
      <script src={bundleSrc} charSet='UTF-8' />
    </body>
  </html>

Html.propTypes = {
  content: PropTypes.string.isRequired,
  styleTags: PropTypes.array.isRequired,
  bundleSrc: PropTypes.string.isRequired,
  client: PropTypes.object.isRequired
}

export default Html
