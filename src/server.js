/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import Koa from 'koa'
import React from 'react'
import { renderToString } from 'react-dom/server'
import { ServerStyleSheet, StyleSheetManager } from 'styled-components'
import webpack from 'webpack'
import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { createHttpLink } from 'apollo-link-http'
import fetch from 'node-fetch'
import { StaticRouter } from 'react-router'
import { ApolloProvider, renderToStringWithData } from 'react-apollo'
import koaWebpack from 'koa-webpack'
import { get } from 'lodash'
import path from 'path'
import koaStatic from 'koa-static'
import historyApiFallback from 'koa-connect-history-api-fallback'
import { SizesProvider } from 'react-sizes'
import { getSizesFallback, cookies } from 'utils'
import Html from './Html'
import config from './config'
import webpackConfig from '../webpack/config.dev'
import Application from './Application'

/**
 * Initialize application
 */
const app = new Koa()

/**
 * Compile webpack
 */
if (process.env.NODE_ENV === 'development') {
  const compiler = webpack(webpackConfig)
  koaWebpack({ compiler, devMiddleware: { serverSideRender: true } })
    .then(middleware => {
      /**
       * Include middlewares
       */
      app
        .use(middleware)
        .use(koaStatic(path.join(__dirname, '../dist')))
        .use(historyApiFallback({ verbose: false }))
        .use(async (ctx) => {
          const token = cookies.get('_gajwt', ctx.headers.cookie)
          console.log(token)
          const client = new ApolloClient({
            ssrMode: true,
            link: createHttpLink({
              uri: config.graphqlURI,
              credentials: 'same-origin',
              headers: {
                Authorization: token ? `Bearer ${token}` : ''
              },
              fetch
            }),
            cache: new InMemoryCache()
          })
          const sheet = new ServerStyleSheet()
          const context = {}
          const sizesConfig = getSizesFallback(ctx.headers['user-agent'])
          const content = await renderToStringWithData(
            <ApolloProvider client={client}>
              <StaticRouter location={ctx.originalUrl} context={context}>
                <StyleSheetManager sheet={sheet.instance}>
                  <SizesProvider config={sizesConfig}>
                    <Application />
                  </SizesProvider>
                </StyleSheetManager>
              </StaticRouter>
            </ApolloProvider>
          )
          const styleTags = sheet.getStyleElement()
          const bundleSrc = get(ctx.state.webpackStats.toJson(), 'assetsByChunkName.main.0')
          const html = (
            <Html
              content={content}
              client={client}
              styleTags={styleTags}
              bundleSrc={bundleSrc}
            />
          )
          ctx.body = `<!doctype html>\n${renderToString(html)}`
        })

        /**
        * Starting server
        */
      app.listen(config.port, (err) => {
        if (err) {
          throw new Error(err)
        }
        console.log(`Server started on http://localhost:${config.port}`)
      })
    })
}
