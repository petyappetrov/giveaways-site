/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Redirect, Link } from 'react-router-dom'
import { compose, graphql, withApollo } from 'react-apollo'
import { renderComponent, branch, withProps } from 'recompose'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInstagram } from '@fortawesome/free-brands-svg-icons'
import { withFormik, Form } from 'formik'
import { get, map } from 'lodash'
import * as yup from 'yup'
import {
  Button,
  Section,
  Container,
  Flex,
  Box,
  Text,
  Field
} from 'components'
import { cookies } from 'utils'
import LoginMutation from 'schemas/LoginMutation.gql'
import MeQuery from 'schemas/MeQuery.gql'

const Login = ({ isSubmitting }) =>
  <Container>
    <Flex justifyContent='center'>
      <Box width={[1, 1, 3/4, 2/4]} mt={[2, 4]}>
        <Section p={[3, 5]}>
          <Flex>
            <Box>
              <Text mb={[1, 2]} fontSize={5} fontWeight='bold'>Войти</Text>
            </Box>
          </Flex>
          <Form>
            <Flex>
              <Box width={1}>
                <Field.Input
                  fullWidth
                  name='email'
                  placeholder='Введите почту'
                />
              </Box>
            </Flex>
            <Flex mt={2}>
              <Box width={1}>
                <Field.Input
                  fullWidth
                  name='password'
                  type='password'
                  placeholder='Введите пароль'
                />
              </Box>
            </Flex>
            <Flex mt={2}>
              <Box width={1}>
                <Button
                  type='submit'
                  width={1}
                  isLoading={isSubmitting}
                >
                  Войти
                </Button>
              </Box>
            </Flex>
            <Flex mt={2}>
              <Box width={1}>
                <Text alignItems='center'>
                  или
                </Text>
              </Box>
              <Box width={1} mt={1}>
                <Button
                  view='instagram'
                  icon={<FontAwesomeIcon icon={faInstagram} size='lg' />}
                  width={1}
                  to='/instagram'
                >
                  Войти через instagram
                </Button>
              </Box>
            </Flex>
            <Flex mt={3}>
              <Box width={1}>
                <Text>
                  Вы ещё не зарегистрированы?<br />
                  <Link to='/register'>Регистрация</Link>
                </Text>
              </Box>
            </Flex>
          </Form>
        </Section>
      </Box>
    </Flex>
  </Container>

Login.propTypes = {
  isSubmitting: PropTypes.bool.isRequired
}

const validationSchema = yup.object().shape({
  email: yup
    .string()
    .email('Некорректный e-mail')
    .required('Пожалуйста введите e-mail'),
  password: yup
    .string()
    .required('Пожалуйста введите пароль')
})

const enhance = compose(
  withApollo,
  withProps(({ client }) => client.readQuery({ query: MeQuery })),
  branch(
    props => props.me,
    renderComponent(({ me }) => <Redirect to={`/${me.slug}`} />)
  ),
  graphql(LoginMutation, { name: 'login' }),
  withFormik({
    validationSchema,
    mapPropsToValues: ({ location }) => ({
      email: get(location, 'state.email', ''),
      password: ''
    }),
    handleSubmit: (variables, { props, setErrors, setSubmitting }) =>
      props.login({ variables })
        .then(({ data }) => cookies.set('_gajwt', data.loginUser.jwt))
        .then(props.client.resetStore)
        .catch((error) => {
          setSubmitting(false)
          map(error.graphQLErrors, ({ extensions }) => {
            const invalidArgs = get(extensions, 'exception.invalidArgs')
            if (invalidArgs) {
              setErrors(invalidArgs)
            }
          })
        })
  })
)

export default enhance(Login)
