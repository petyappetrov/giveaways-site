/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Link, withRouter } from 'react-router-dom'
import { withApollo } from 'react-apollo'
import { isEmpty } from 'lodash'
import {
  Button,
  Section,
  Avatar,
  Gradient,
  Flex,
  Box,
  Container,
  Table,
  Text,
  ModalWithState
} from 'components'
import { cookies } from 'utils'
import { compose, withHandlers, withProps } from 'recompose'
import MeQuery from 'schemas/MeQuery.gql'

const User = ({ isMe, logout, data }) => {
  const { photoUrl, about, email, contests, name } = data
  return (
    <Fragment>
      <Gradient>
        <Container>
          <Flex pt={60}>
            <Box width={2/12}>
              <Avatar src={photoUrl} />
            </Box>
            <Box width={10/12} pt={2}>
              <Flex flexDirection='column'>
                <Box>
                  <Text color='white' fontSize={5} fontWeight='bold'>{name}</Text>
                </Box>
                <Box>
                  <Text color='#fff' fontSize={2}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  </Text>
                </Box>
                {isMe &&
                  <Box mt={15}>
                    <ModalWithState>
                      {({ openModal, closeModal, content }) =>
                        <Fragment>
                          <Button view='other' onClick={openModal}>
                            Редактировать
                          </Button>
                          {content(
                            <div>
                              Hello
                              <Button onClick={closeModal}>Отмена</Button>
                            </div>
                          )}
                        </Fragment>
                      }
                    </ModalWithState>
                    <Button
                      view='other'
                      onClick={logout}
                      ml={15}
                    >
                      Выйти
                    </Button>
                  </Box>
                }
              </Flex>
            </Box>
          </Flex>
        </Container>
      </Gradient>
      <Container>
        <Flex mt={-45}>
          <Box width={[1, 1, 1, 1/3]} pb={15}>
            <Section>
              <Section.Title>
                Контакты
              </Section.Title>
              <Flex justifyContent='center'>
                <Box text='center' width={1}>
                  {about &&
                    <Text mt={15}>{about}</Text>
                  }
                  <Text>{email}</Text>
                  Привязанные аккаунты:
                  <Text>
                    <a href='http://instagram.com'>instagram</a>
                  </Text>
                </Box>
              </Flex>
            </Section>
          </Box>
          <Box width={[1, 1, 1, 2/3]}>
            <Section>
              <Section.Title>
                Конкурсы
              </Section.Title>
              {!isEmpty(contests.items)
                ? (
                  <Table
                    data={contests.items}
                    columns={[
                      {
                        header: 'Название',
                        accessor: 'title',
                        transform: (col, Flex) => <Link to={`/contests/${Flex._id}`}>{col}</Link>
                      },
                      {
                        header: 'Тип',
                        accessor: 'type'
                      },
                      {
                        header: 'Социальная сеть',
                        accessor: 'socialNetwork'
                      },
                      {
                        header: 'Создано',
                        accessor: 'createdAt'
                      },
                      {
                        header: 'Конец',
                        accessor: 'endDate'
                      }
                    ]}
                  />
                )
                : (
                  <p>
                    У вас пока нет конкурсов.
                    {' '}
                    <Link to='/create'>Создать свой первый конкурс.</Link>
                  </p>
                )
              }
            </Section>
          </Box>
        </Flex>
      </Container>
    </Fragment>
  )
}

User.propTypes = {
  isMe: PropTypes.bool,
  logout: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired
}

const enhance = compose(
  withRouter,
  withApollo,
  withProps(({ client, data }) => {
    const { me } = client.readQuery({ query: MeQuery })
    return {
      isMe: me && me.slug === data.slug
    }
  }),
  withHandlers({
    logout: ({ client, history }) => async () => {
      await cookies.remove('_gajwt')
      await client.resetStore()
      history.push('/login')
    }
  })
)

export default enhance(User)
