/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Redirect, Link } from 'react-router-dom'
import { compose, graphql, withApollo } from 'react-apollo'
import { renderComponent, branch, withProps } from 'recompose'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInstagram } from '@fortawesome/free-brands-svg-icons'
import { withFormik, Form } from 'formik'
import { get, map } from 'lodash'
import * as yup from 'yup'
import {
  Button,
  Section,
  Container,
  Flex,
  Box,
  Text,
  Field
} from 'components'
import { cookies } from 'utils'
import LoginInstagramMutation from 'schemas/LoginInstagramMutation.gql'
import MeQuery from 'schemas/MeQuery.gql'

const LoginInstagram = ({ isSubmitting }) =>
  <Container>
    <Flex justifyContent='center'>
      <Box width={[1, 1, 3/4, 2/4]} mt={[2, 4]}>
        <Section p={[3, 5]}>
          <Flex>
            <Box>
              <Text mb={[1, 2]} fontSize={5} fontWeight='bold'>Instagram</Text>
            </Box>
          </Flex>
          <Form>
            <Flex>
              <Box width={1}>
                <Field.Input
                  fullWidth
                  name='name'
                  placeholder='Введите имя аккаунта'
                />
              </Box>
            </Flex>
            <Flex mt={2}>
              <Box width={1}>
                <Field.Input
                  fullWidth
                  name='password'
                  type='password'
                  placeholder='Введите пароль'
                />
              </Box>
            </Flex>
            <Flex mt={2}>
              <Box width={1}>
                <Button
                  isLoading={isSubmitting}
                  type='submit'
                  view='instagram'
                  icon={<FontAwesomeIcon icon={faInstagram} size='lg' />}
                  width={1}
                >
                  Войти через instagram
                </Button>
              </Box>
            </Flex>
            <Flex mt={3}>
              <Box width={1}>
                <Text>
                  Нажимая кнопнку &laquo;Войти через instagram&raquo;,
                  вы&nbsp;соглашаетесь<br />
                  с&nbsp;<Link to='/terms'>Условиями использования</Link> Giveaways.su
                </Text>
              </Box>
            </Flex>
          </Form>
        </Section>
      </Box>
    </Flex>
  </Container>

LoginInstagram.propTypes = {
  isSubmitting: PropTypes.bool.isRequired
}

const validationSchema = yup.object().shape({
  name: yup
    .string()
    .required('Пожалуйста введите имя аккаунта'),
  password: yup
    .string()
    .required('Пожалуйста введите пароль')
})

const enhance = compose(
  withApollo,
  withProps(({ client }) => client.readQuery({ query: MeQuery })),
  branch(
    props => props.me,
    renderComponent(({ me }) => <Redirect to={`/${me.slug}`} />)
  ),
  graphql(LoginInstagramMutation, { name: 'login' }),
  withFormik({
    validationSchema,
    mapPropsToValues: ({ location }) => ({
      name: '',
      password: ''
    }),
    handleSubmit: (variables, { props, setErrors, setSubmitting }) =>
      props.login({ variables })
        .then(({ data }) => cookies.set('_gajwt', data.loginInstagram.jwt))
        .then(props.client.resetStore)
        .catch((error) => {
          setSubmitting(false)
          map(error.graphQLErrors, ({ extensions }) => {
            const invalidArgs = get(extensions, 'exception.invalidArgs')
            if (invalidArgs) {
              setErrors(invalidArgs)
            }
          })
        })
  })
)

export default enhance(LoginInstagram)
