/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { withApollo } from 'react-apollo'
import { withProps, compose } from 'recompose'
import { Flex, Box, Gradient, Text, Container, Button, Section } from 'components'
import MeQuery from 'schemas/MeQuery.gql'

const Hero = styled.div`
  width: 100%;
  color: #fff;
  position: relative;
  margin-top: -90px;
`

const Home = ({ me }) => {
  return (
    <Fragment>
      <Hero>
        <Gradient height={540} name='Deep Sea'>
          <Container>
            <Flex justifyContent='center' align='center'>
              <Box text='center' width={1}>
                <Text mt={[200, 220]} mb={[0, 1]} px={[15, 15]} fontSize={5} fontWeight='bold'>
                  Теперь организовывать конкурсы в социальных сетях – просто и легко!
                </Text>
                {me
                  ? (
                    <Fragment>
                      <Text fontSize={[2, 3]} p={20}>
                        Попробуйте совершенно бесплатно
                      </Text>
                      <Button view='other' to='/create'>Создать конкурс</Button>
                    </Fragment>
                  )
                  : (
                    <Fragment>
                      <Text fontSize={[2, 3]} p={20}>
                        Зарегистрируйтесь и попробуйте совершенно бесплатно
                      </Text>
                      <br />
                      <Button view='other' to='/register'>Зарегистрироваться</Button>
                    </Fragment>
                  )
                }
              </Box>
            </Flex>
          </Container>
        </Gradient>
      </Hero>
      <Container>
        <Section mt={-45}>
          <Flex pb={2}>
            <Box width={1}>
              <Section.Title mb={[0, 1]}>
                Как это работает
              </Section.Title>
              <Text>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Quos quo iure natus accusantium earum sapiente molestiae doloremque?
                Dolor omnis suscipit laudantium ab maiores provident sapiente! Perferendis aut,
                officia ipsum totam error eos pariatur perspiciatis quaerat animi repellendus
                necessitatibus dignissimos facilis porro unde alias voluptate id molestiae nisi
                asperiores quasi eaque eligendi, deserunt aspernatur, expedita! Incidunt vitae
                consectetur sint earum architecto, non laudantium, excepturi est fugiat officia quia!
                Pariatur itaque, atque aperiam facilis aliquam fugit, eveniet maiores cumque
                doloremque animi suscipit sed ducimus recusandae unde placeat laudantium, optio
                repellendus nam beatae quo, velit! Dignissimos vitae, expedita deserunt officiis
                voluptatum! Nam, fuga.
              </Text>
            </Box>
          </Flex>
        </Section>
      </Container>
    </Fragment>
  )
}

Home.propTypes = {
  me: PropTypes.object
}

const enhance = compose(
  withApollo,
  withProps(({ client, match }) => client.readQuery({ query: MeQuery }))
)

export default enhance(Home)
