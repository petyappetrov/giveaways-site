/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

export { default as Home } from './Home'
export { default as Login } from './Login'
export { default as LoginInstagram } from './LoginInstagram'
export { default as Register } from './Register'
export { default as User } from './User'
export { default as Contests } from './Contests'
export { default as Contest } from './Contest'
export { default as Slugger } from './Slugger'
export { default as NotFound } from './NotFound'
