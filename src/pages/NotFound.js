/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import { Flex, Box, Container, Text } from 'components'

const NotFound = () =>
  <Container>
    <Flex justifyContent='center' textAlign='center' mt={4}>
      <Box>
        <Text fontSize={5} fontWeight='bold'>
          Ошибка четыреста четыре...
        </Text>
        <Text>
          Конкурс или пользователь не найден.<br />
          Рекомендуем воспользоваться поиском.
        </Text>
      </Box>
    </Flex>
  </Container>

export default NotFound
