/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Redirect, Link } from 'react-router-dom'
import { compose, graphql, withApollo } from 'react-apollo'
import { omit, map, get } from 'lodash'
import { renderComponent, branch, withProps } from 'recompose'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInstagram } from '@fortawesome/free-brands-svg-icons'
import { withFormik, Form } from 'formik'
import * as yup from 'yup'
import {
  Button,
  Field,
  Section,
  Box,
  Container,
  Flex,
  Text
} from 'components'
import RegisterMutation from 'schemas/RegisterMutation.gql'
import MeQuery from 'schemas/MeQuery.gql'

const Register = ({ isSubmitting }) =>
  <Container>
    <Flex justifyContent='center'>
      <Box width={[1, 1, 3/4, 2/4]} mt={[2, 4]}>
        <Section p={[3, 5]}>
          <Flex>
            <Box>
              <Text mb={[1, 2]} fontSize={5} fontWeight='bold'>Регистрация</Text>
            </Box>
          </Flex>
          <Form>
            <Flex>
              <Box width={1}>
                <Field.Input
                  fullWidth
                  name='email'
                  placeholder='Введите почту'
                />
              </Box>
            </Flex>
            <Flex mt={2}>
              <Box width={1}>
                <Field.Input
                  fullWidth
                  name='name'
                  placeholder='Введите имя'
                />
              </Box>
            </Flex>
            <Flex mt={2}>
              <Box width={1}>
                <Field.Input
                  fullWidth
                  type='password'
                  name='password'
                  placeholder='Введите пароль'
                />
              </Box>
            </Flex>
            <Flex mt={2}>
              <Box width={1}>
                <Button
                  type='submit'
                  width={1}
                  isLoading={isSubmitting}
                >
                  Регистрация
                </Button>
              </Box>
            </Flex>
            <Flex mt={2}>
              <Box width={1}>
                <Text align='center'>
                  или
                </Text>
              </Box>
              <Box width={1} mt={1}>
                <Button
                  view='instagram'
                  icon={<FontAwesomeIcon icon={faInstagram} size='lg' />}
                  width={1}
                  to='/instagram'
                >
                  Войти через instagram
                </Button>
              </Box>
            </Flex>
            <Flex mt={3}>
              <Box width={1}>
                <Text>
                  Нажимая кнопнку &laquo;Регистрация&raquo;,
                  вы&nbsp;соглашаетесь<br />
                  с&nbsp;<Link to='/terms'>Условиями использования</Link> Giveaways.su
                </Text>
              </Box>
            </Flex>
          </Form>
        </Section>
      </Box>
    </Flex>
  </Container>

Register.propTypes = {
  isSubmitting: PropTypes.bool.isRequired
}

const validationSchema = yup.object({
  email: yup
    .string()
    .email('Некорректный e-mail')
    .required('Пожалуйста введите e-mail'),
  name: yup
    .string()
    .required('Пожалуйста введите ваше имя'),
  password: yup
    .string()
    .required('Пожалуйста придумайте пароль')
})

const enhance = compose(
  withApollo,
  withProps(({ client }) => client.readQuery({ query: MeQuery })),
  branch(
    props => props.me,
    renderComponent(({ me }) => <Redirect to={`/${me.slug}`} />)
  ),
  graphql(RegisterMutation, { name: 'registerUser' }),
  withFormik({
    validationSchema,
    mapPropsToValues: () => ({
      email: '',
      name: '',
      password: ''
    }),
    handleSubmit: (values, { props, setErrors, setSubmitting }) =>
      props.registerUser({ variables: omit(values, 'confirm') })
        .then(({ data }) =>
          props.history.push({
            pathname: '/login',
            state: {
              email: data.registerUser.email
            }
          }))
        .catch(({ graphQLErrors }) => {
          setSubmitting(false)
          map(graphQLErrors, ({ extensions }) => {
            const invalidArgs = get(extensions, 'exception.invalidArgs')
            if (invalidArgs) {
              setErrors(invalidArgs)
            }
          })
        })
  })
)

export default enhance(Register)
