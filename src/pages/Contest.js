/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Container, Flex, Box } from 'components'

const Contest = ({ data }) =>
  <Container>
    <Flex>
      <Box>{data.title}</Box>
    </Flex>
  </Container>

Contest.propTypes = {
  data: PropTypes.object.isRequired
}

export default Contest
