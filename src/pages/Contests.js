/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import ContestsQuery from 'schemas/ContestsQuery.gql'
import { Query } from 'react-apollo'
import { withStateHandlers, compose } from 'recompose'
import { map } from 'lodash'
import { Container, Spinner, Flex, Box, Select, ContestItem, Masonry, Button } from 'components'
import { sortVariables } from 'utils'

const Contests = ({ variables, setVariable, resetVariables, setSort, sort }) =>
  <Container>
    <Flex mt={4} justifyContent='space-between' alignItems='center'>
      <Box>
        <Select
          label='Социальная сеть'
          name='socialNetwork'
          mr={2}
          onChange={(socialNetwork) =>
            setVariable({ filters: { ...variables.filters, socialNetwork } })
          }
          value={variables.filters.socialNetwork}
          options={[
            {
              label: 'instagram',
              value: 'instagram'
            },
            {
              label: 'twitter',
              value: 'twitter'
            }
          ]}
        />
        <Select
          label='Город'
          name='city'
          mr={2}
          onChange={(city) =>
            setVariable({ filters: { ...variables.filters, city } })
          }
          value={variables.filters.city}
          options={[
            {
              label: 'Москва',
              value: 'msk'
            },
            {
              label: 'Санкт-Петербург',
              value: 'spb'
            }
          ]}
        />
        <Select
          name='status'
          onChange={(status) => setVariable({ status })}
          value={variables.status}
          options={[
            {
              label: 'Только активные',
              value: 'active'
            },
            {
              label: 'Только завершенные',
              value: 'inactive'
            }
          ]}
        />
      </Box>
      <Box>
        <Select
          name='sort'
          onChange={setSort}
          value={sort}
          options={[
            {
              label: 'Сначала новые',
              value: 'newest'
            },
            {
              label: 'Сначала завершающиеся',
              value: 'ending'
            }
          ]}
        />
      </Box>
    </Flex>
    <Query
      query={ContestsQuery}
      variables={variables}
      fetchPolicy='cache-first'
    >
      {({ data, loading, fetchMore }) => {
        if (loading) {
          return <Spinner />
        }
        return (
          <Flex mt={3}>
            <Masonry>
              {map(data.contests.items, (item) =>
                <Box key={item._id} width={328} pb={3}>
                  <ContestItem {...item} />
                </Box>
              )}
            </Masonry>
            {data.contests.count > data.contests.items.length &&
              <Button
                onClick={() =>
                  fetchMore({
                    variables: {
                      skip: data.contests.items.length
                    },
                    updateQuery: (prev, { fetchMoreResult }) => {
                      if (!fetchMoreResult) {
                        return prev
                      }
                      return {
                        ...prev,
                        contests: {
                          ...prev.contests,
                          items: [
                            ...prev.contests.items,
                            ...fetchMoreResult.contests.items
                          ]
                        }
                      }
                    }
                  })
                }
              >
                Загрузить еще
              </Button>
            }
          </Flex>
        )
      }}
    </Query>
  </Container>

Contests.propTypes = {
  variables: PropTypes.object.isRequired,
  sort: PropTypes.oneOf(['newest', 'ending']),
  setSort: PropTypes.func.isRequired,
  setVariable: PropTypes.func.isRequired,
  resetVariables: PropTypes.func.isRequired
}

const enhance = compose(
  withStateHandlers(
    {
      variables: {
        status: 'active',
        filters: {}
      }
    },
    {
      setSort: ({ variables }) => (sort) => ({
        sort,
        variables: {
          ...variables,
          ...sortVariables[sort]
        }
      }),
      setVariable: ({ variables }) => (variable) => ({
        variables: {
          ...variables,
          ...variable
        }
      }),
      resetVariables: () => () => ({ variables: {} })
    }
  )
)

export default enhance(Contests)
