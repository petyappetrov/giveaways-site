/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Query } from 'react-apollo'
import { User, Contest, NotFound } from 'pages'
import { Spinner } from 'components'
import SlugQuery from 'schemas/SlugQuery.gql'

const Slugger = ({ match }) =>
  <Query query={SlugQuery} variables={{ slug: match.params.slug }}>
    {({ data, loading }) => {
      if (loading) {
        return <Spinner />
      }
      switch (data.slug && data.slug.__typename) {
        case 'User': {
          return <User data={data.slug} />
        }
        case 'Contest': {
          return <Contest data={data.slug} />
        }
        default: {
          return <NotFound />
        }
      }
    }}
  </Query>

Slugger.propTypes = {
  match: PropTypes.object.isRequired
}

export default Slugger
