/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

const fetch = require('node-fetch')
const fs = require('fs')
const config = require('./config')

const query = `{
  __schema {
    types {
      kind
      name
      possibleTypes {
        name
      }
    }
  }
}`

const generateFragmentTypes = () =>
  fetch(config.graphqlURI, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ query })
  })
    .then(async response => {
      const { data } = await response.json()
      const types = data.__schema.types.filter(type => type.possibleTypes !== null)

      return new Promise((resolve, reject) => {
        fs.writeFile('./src/fragment-types.json', JSON.stringify({ __schema: { types } }), err => {
          if (err) {
            reject(err)
          } else {
            console.log('Fragment types successfully extracted!')
            resolve()
          }
        })
      })
    })
    .catch(console.log)

/**
 * Extracting the necessary information from the schema into a JSON file
 */
generateFragmentTypes().then(() => {
  require('babel-core/register')
  require('./server')
})
