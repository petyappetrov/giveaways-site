/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React, { Fragment } from 'react'
import { Route, Switch } from 'react-router-dom'
import { hot } from 'react-hot-loader'
import { Query } from 'react-apollo'
import { ThemeProvider } from 'styled-components'
import { theme } from 'theme'
import { Spinner, Header, Footer, Box } from 'components'
import { Home, Login, Register, LoginInstagram, Contests, Slugger } from 'pages'

import MeQuery from 'schemas/MeQuery.gql'

const Application = () =>
  <Query query={MeQuery}>
    {({ data, loading }) => {
      if (loading) {
        return <Spinner />
      }
      return (
        <ThemeProvider theme={theme}>
          <Fragment>
            <Header me={data.me} />
            <Box flex='1 0 auto' is='main' px='0'>
              <Switch>
                <Route path='/' component={Home} exact />
                <Route path='/login' component={Login} />
                <Route path='/register' component={Register} />
                <Route path='/instagram' component={LoginInstagram} />
                <Route path='/contests' component={Contests} />
                <Route path='/:slug' component={Slugger} />
              </Switch>
            </Box>
            <Footer />
          </Fragment>
        </ThemeProvider>
      )
    }}
  </Query>

export default hot(module)(Application)
