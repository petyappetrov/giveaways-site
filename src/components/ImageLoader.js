/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Spinner } from 'components'

const STATUS_LOADING = 'LOADING'
const STATUS_LOADED = 'LOADED'
const STATUS_ERROR = 'ERROR'

const ImageWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  width: ${p => p.width + 'px' || '100%'};
  height: ${p => p.height + 'px' || '100%'};
`

const Image = styled.img`
  object-fit: cover;
  width: 100%;
  height: 100%;
  ${p => {
    switch (p.status) {
      case STATUS_LOADED:
        return `
          display: block;
        `
      case STATUS_LOADING:
      case STATUS_ERROR:
      default:
        return `
          display: none;
        `
    }
  }}
`

class ImageLoader extends React.Component {
  state = {
    status: STATUS_LOADING
  }

  onLoad = () => {
    this.setState({
      status: STATUS_LOADED
    })
  }

  onError = () => {
    this.setState({
      status: STATUS_ERROR
    })
  }

  render () {
    const { src, width, height, alt } = this.props
    return (
      <ImageWrapper width={width} height={height}>
        <Image
          src={src}
          alt={alt}
          onLoad={this.onLoad}
          onError={this.onError}
          status={this.state.status}
        />
        {this.state.status === STATUS_LOADING &&
          <Spinner />
        }
      </ImageWrapper>
    )
  }
}

ImageLoader.propTypes = {
  src: PropTypes.string,
  alt: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number
}

export default ImageLoader
