/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { map, max, min, isNumber, filter, last, isEmpty, isEqual } from 'lodash'
import MasonryItem from './MasonryItem'

const getItemHeight = (item) => {
  const child = item.firstChild
  const candidate = filter([child.scrollHeight, child.clientHeight, child.offsetHeight, 0], isNumber)
  return max(candidate)
}

class Masonry extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  }

  state = {
    rectangles: []
  }

  componentDidMount () {
    this.updateLayout()
  }

  componentDidUpdate () {
    this.updateLayout()
  }

  shouldComponentUpdate = (nextProps, nextState) => {
    return (
      !isEqual(nextProps, this.props) ||
      !isEqual(nextState, this.state)
    )
  }

  updateLayout = () => {
    if (!this.node) {
      return null
    }
    const containerWidth = this.node.offsetWidth
    const width = this.node.children[0].offsetWidth
    const maxColumn = parseInt(containerWidth / width)
    const columnHeights = Array(maxColumn).fill(0)

    const rectangles = map(this.node.children, children => {
      const index = columnHeights.indexOf(min(columnHeights))

      const height = getItemHeight(children)
      const left = index * width
      const top = columnHeights[index]

      columnHeights[index] += height

      return { top, left, width, height }
    })

    this.setState({ rectangles })
  }

  render () {
    if (isEmpty(this.props.children)) {
      return null
    }
    const lastItem = last(this.state.rectangles)
    return (
      <div
        ref={node => (this.node = node)}
        style={{
          width: '100%',
          position: 'relative',
          height: lastItem ? lastItem.top + lastItem.height + 50 : 'auto'
        }}>
        {React.Children.map(this.props.children, (children, i) =>
          <MasonryItem
            updateLayout={this.updateLayout}
            rectangle={this.state.rectangles[i]}
          >
            {children}
          </MasonryItem>
        )}
      </div>
    )
  }
}

export default Masonry
