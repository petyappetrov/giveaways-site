/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { isEqual } from 'lodash'

class MasonryItem extends React.Component {
  static propTypes = {
    updateLayout: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
    rectangle: PropTypes.object
  }

  componentDidMount () {
    const images = Array.prototype.slice.call(this.node.getElementsByTagName('img'))
    if (images.length) {
      this.monitorImagesLoaded(images)
    }
  }

  shouldComponentUpdate = (nextProps) => {
    return !isEqual(nextProps, this.props)
  }

  monitorImagesLoaded = async (images) => {
    await Promise.all(images.map(image =>
      new Promise((resolve, reject) => {
        image.onload = resolve
        image.onerror = reject
      })
    ))
    this.props.updateLayout()
  }

  render () {
    return (
      <div
        ref={node => (this.node = node)}
        style={{
          position: 'absolute',
          ...this.props.rectangle
        }}
      >
        {this.props.children}
      </div>
    )
  }
}

export default MasonryItem
