/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'
import tag from 'clean-tag'
import { space, width, flex } from 'styled-system'

const Box = styled(tag).attrs({
  px: p => p.px || 2
})`
  flex: 0 0 auto;
  box-sizing: border-box;
  ${space}
  ${width}
  ${flex}
`

export default Box
