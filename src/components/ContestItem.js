/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

const Item = styled(Link)`
  width: 100%;
  border-radius: 10px;
  overflow: hidden;
  background: #fff;
  box-sizing: border-box;
  display: block;
  color: #000;
  height: auto;
  box-shadow: ${p => p.theme.shadow};
`

const Image = styled.div`
  height: auto;
  overflow: hidden;
  background: #e4e4e4;
  min-height: 180px;
  > img {
    object-fit: cover;
    width: 100%;
    height: 100%;
  }
`

const Content = styled.div`
  padding: 15px;
`

const Title = styled.div`
  font-size: 22px;
  font-weight: 700;
  text-transform: uppercase;
  margin: 0 0 10px;
  word-wrap: break-word;
`

const Description = styled.div`
  &:first-letter {
    text-transform: uppercase;
  }
`

const ContestItem = ({ type, imageUrl, title, city, slug, endDate, createdAt }) =>
  <Item to={`/${slug}`}>
    <Image>
      <img src={imageUrl} />
    </Image>
    <Content>
      <Title>{title}</Title>
      <Description>
        <div>
          Создано: {createdAt}
        </div>
        <div>
          Конец конкурса: {endDate}
        </div>
      </Description>
    </Content>
  </Item>

ContestItem.propTypes = {
  type: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  endDate: PropTypes.string.isRequired,
  createdAt: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired
}

export default ContestItem
