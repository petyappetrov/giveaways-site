/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { space, width, responsiveStyle } from 'styled-system'
import { Spinner } from 'components'

const order = responsiveStyle('order')

const views = {
  default: `
    background-color: #7F7AF9;
    color: #fff;
    &:hover {
      background-color: #8E8AF7;
    }
  `,
  primary: `
    background: linear-gradient(to right, rgb(130, 200, 146), rgb(150, 207, 135));
    box-shadow: 0 5px 40px rgba(29, 34, 50, .06), 0 10px 90px 4px rgba(29, 34, 50, .08);
    color: #fff;
    &:hover {
      background-color: rgb(147, 202, 147);
    }
  `,
  ghost: `
    background-color: transparent;
    color: #7F7AF9;
    border: 1px solid #7F7AF9;
    line-height: 38px;
    &:hover {
      background-color: #7F7AF9;
      border-color: #7F7AF9;
      color: #fff;
    }
  `,
  other: `
    background-color: transparent;
    color: #fff;
    border: 1px solid #fff;
    line-height: 38px;
    &:hover {
      background-color: #fff;
      color: #000;
    }
  `,
  danger: `
    background-color: #CD0F21;
    color: #fff;
    &:hover {
      background-color: #e43848;
    }
  `,
  instagram: `
    background: linear-gradient(45deg, #f09433 0%,#e6683c 25%,#dc2743 50%,#cc2366 75%,#bc1888 100%);
    color: #fff;
    &:hover {
      background: linear-gradient(45deg, #f1a24f 0%,#e9744a 25%,#e03650 50%,#d73474 75%,#c62a95 100%);
    }
  `
}

const buttonCSS = css`
  ${space}
  ${width}
  ${order}
  display: inline-block;
  height: 40px;
  line-height: 40px;
  font-size: 1rem;
  border-radius: 10px;
  box-sizing: border-box;
  border: none;
  cursor: pointer;
  padding: 0 20px;
  user-select: none;
  &:disabled {
    opacity: 0.55;
    pointer-events: none;
    cursor: default;
  }
  ${(p) => views[p.view]}
`

const ButtonStyled = styled.button`
  ${buttonCSS}
  ${p => p.to && `
    font-weight: normal;
    &:hover {
      text-decoration: none;
    }
  `}
`

const Icon = styled.span`
  margin-right: 10px;
  margin-left: -5px;
`

class Button extends React.Component {
  static propTypes = {
    onClick: PropTypes.func,
    to: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object
    ]),
    view: PropTypes.oneOf([
      'default',
      'primary',
      'ghost',
      'other',
      'danger',
      'instagram'
    ]),
    type: PropTypes.string,
    disabled: PropTypes.bool,
    isLoading: PropTypes.bool,
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ]),
    icon: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
    ])
  }

  static contextTypes = {
    router: PropTypes.shape({
      history: PropTypes.shape({
        push: PropTypes.func.isRequired
      }).isRequired
    }).isRequired
  }

  static defaultProps = {
    view: 'default',
    type: 'button',
    isLoading: false,
    disabled: false
  }

  handleClick = (event) => {
    if (this.props.onClick) {
      this.props.onClick(event)
    }
    if (this.props.to) {
      event.preventDefault()
      const { history } = this.context.router
      history.push(this.props.to)
    }
  }

  render () {
    const { children, icon, isLoading, disabled, ...props } = this.props
    return (
      <ButtonStyled
        {...props}
        disabled={isLoading || disabled}
        onClick={this.handleClick}
      >
        {isLoading
          ? <Spinner />
          : (
            <React.Fragment>
              {icon && <Icon>{icon}</Icon>}
              {children}
            </React.Fragment>
          )
        }
      </ButtonStyled>
    )
  }
}

export default Button
