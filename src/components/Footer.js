/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import styled from 'styled-components'
import { space } from 'styled-system'
import { Link } from 'react-router-dom'
import { Flex, Box, Container, Text } from 'components'

const Footer = styled.footer`
  ${space}
  width: 100%;
  font-size: 14px;
  background: #fff;
  box-shadow: ${p => p.theme.shadow};
  a {
    color: #343E5F;
    font-weight: 700;
    &:hover {
      text-decoration: underline;
    }
  }
`

export default () =>
  <Footer py={20} mt={[15, 30, 80]}>
    <Container>
      <Text alignItems={['center', 'left']}>
        <Flex>
          <Box width={[1, 1/2, 1/2, 1/4]} py={[3, 2, 2, 0]}>
            <Link to='/rules'>Правила и условия публикации</Link><br />
            <Link to='/faq'>Часто задаваемые вопросы</Link>
          </Box>
          <Box width={[1, 1/2, 1/2, 1/4]} py={[3, 2, 2, 0]}>
            <Link to='/terms'>Условия использования</Link><br />
            <Link to='/privacy'>Политика конфиденциальности</Link>
          </Box>
          <Box width={[1, 1/2, 1/2, 1/4]} py={[3, 2, 2, 0]}>
            По просьбам и предложениям: <br />
            <a href='mailto:petyappetrov@yandex.ru'>petyappetrov@yandex.ru</a>
          </Box>
          <Box width={[1, 1/2, 1/2, 1/4]} py={[3, 2, 2, 0]}>
            Разработка сайта –
            {' '}
            <a
              href='https://petyappetrov.github.io'
              target='_blank'
              rel='noopener noreferrer'
            >
              Петр Петров
            </a><br />
            {new Date().getFullYear()} &copy; Все права защищены
          </Box>
        </Flex>
      </Text>
    </Container>
  </Footer>
