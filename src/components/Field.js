/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Field } from 'formik'
import { Input, Text, Label } from 'components'

Field.Input = ({ name, label, ...props }) =>
  <Field
    name={name}
    render={({
      field,
      form: {
        touched,
        errors
      }
    }) =>
      <Fragment>
        {
          label &&
            <Label
              htmlFor={name}
              error={touched[name] && errors[name]}
            >
              {label}
            </Label>
        }
        <Input
          name={name}
          error={touched[name] && errors[name]}
          {...field}
          {...props}
        />
        {
          touched[name] && errors[name] &&
            <Text error>{touched[name] && errors[name]}</Text>
        }
      </Fragment>
    }
  />

Field.Input.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string
}

export default Field
