/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'

const Label = styled.label`
  display: block;
  color: #333;
  padding-bottom: 5px;
  ${(p) => p.error && 'color: #CD0F21;'}
`

export default Label
