/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import { withStateHandlers } from 'recompose'
import { Modal } from 'components'

const ModalWithState = ({ children, openModal, closeModal, isOpen }) =>
  children({
    openModal,
    closeModal,
    content: children => {
      if (!isOpen) {
        return null
      }
      return (
        <Modal isOpen closeModal={closeModal}>
          {children}
        </Modal>
      )
    }
  })

const enhance = withStateHandlers(
  {
    isOpen: false
  },
  {
    openModal: () => () => ({
      isOpen: true
    }),
    closeModal: () => () => ({
      isOpen: false
    })
  }
)

export default enhance(ModalWithState)
