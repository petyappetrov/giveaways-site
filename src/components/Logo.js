/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGift } from '@fortawesome/free-solid-svg-icons'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

const LogoLink = styled(Link)`
  font-size: 22px;
  font-weight: bold;
  color: #343E5F;
  position: relative;
  z-index: 1;
  &:hover {
    text-decoration: none;
  }
`

const Logo = () =>
  <LogoLink to='/'>
    <FontAwesomeIcon icon={faGift} />&nbsp;
    giveaways.su
  </LogoLink>

export default Logo
