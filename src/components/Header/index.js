/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Container, Flex, Box, Logo } from 'components'
import styled from 'styled-components'
import Menu from './Menu'
import Search from './Search'

const HeaderLayout = styled.header`
  width: 100%;
  height: 70px;
  display: flex;
  flex-shrink: 0;
  align-items: center;
  z-index: 10;
  position: relative;
  background-color: #fff;
  box-shadow: ${p => p.theme.shadow};
`

const Header = ({ me }) =>
  <HeaderLayout>
    <Container>
      <Flex justifyContent='space-between' alignItems='center'>
        <Box>
          <Flex alignItems='center'>
            <Box mr={4}>
              <Logo />
            </Box>
            <Box>
              <Search />
            </Box>
          </Flex>
        </Box>
        <Box>
          <Menu me={me} />
        </Box>
      </Flex>
    </Container>
  </HeaderLayout>

Header.propTypes = {
  me: PropTypes.object
}

export default Header
