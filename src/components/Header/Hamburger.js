/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import styled, { css } from 'styled-components'

const Meat = styled.div`
  top: 50%;
  display: block;
  &,
  &::after,
  &::before {
    content: '';
    position: absolute;
    left: 0;
    width: 26px;
    height: 2px;
    transition-timing-function: cubic-bezier(.55,.055,.675,.19);
    transition-duration: .22s;
    transition-property: transform;
    border-radius: 2px;
    background-color: #fff;
  }
  &::before {
    top: -8px;
    transition: top .1s ease-in .25s,opacity .1s ease-in;
  }
  &::after {
    bottom: -8px;
    transition: bottom .1s ease-in .25s, transform .22s cubic-bezier(.55,.055,.675,.19);
  }
`

const activeCSS = css`
  ${Meat} {
    transition-delay: .12s;
    transition-timing-function: cubic-bezier(.215,.61,.355,1);
    transform: rotate(225deg);
    &::before {
      top: 0;
      transition: top .1s ease-out,opacity .1s ease-out .12s;
      opacity: 0;
    }
    &::after {
      bottom: 0;
      transition: bottom .1s ease-out, transform .22s cubic-bezier(.215,.61,.355,1) .12s;
      transform: rotate(-90deg);
    }
  }
`

const Hamburger = styled.div`
  position: relative;
  cursor: pointer;
  z-index: 1;
  display: inline-block;
  width: 26px;
  height: 22px;
  ${p => p.active && activeCSS}
`

export default (props) =>
  <Hamburger {...props}>
    <Meat />
  </Hamburger>
