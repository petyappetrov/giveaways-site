/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import { withSizes } from 'react-sizes'
import { withStateHandlers, compose, withHandlers } from 'recompose'
import styled, { css } from 'styled-components'
import onClickOutside from 'react-onclickoutside'
import { Button, Flex, Box } from '../'
import Hamburger from './Hamburger'

const MOBILE_WIDTH = 1024

const MenuItem = styled(NavLink)`
  margin-right: 40px;
  cursor: pointer;
  color: #343E5F;
  &:hover {
    text-decoration: none;
  }
`

const MobileCSS = css`
  position: absolute;
  flex-direction: column;
  text-align: center;
  top: 0;
  left: 0;
  width: 100%;
  background: #000;
  padding: 100px 0 40px;
  box-sizing: border-box;
  justify-content: center;
  opacity: 0;
  visibility: hidden;
  transition: opacity 0.30s ease,
              visibility 0s linear 0.30s,
              transform 0.30s ease;
  transform: translate3d(0, -30px, 0);
  pointer-events: none;
  ${MenuItem}, a {
    margin: 15px 0;
  }

  ${p => p.show && `
    opacity: 1;
    transform: translate3d(0, 0, 0);
    transition-delay: 0s;
    visibility: visible;
    pointer-events: auto;
  `}
`

const MenuContent = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  ${p => p.isMobile && MobileCSS}
`

const Menu = ({ isMobile, me, show, toggleMenu }) =>
  <Flex>
    <Box>
      {isMobile &&
        <Hamburger
          onClick={toggleMenu}
          active={show}
        />
      }
      <MenuContent show={show} isMobile={isMobile}>
        <MenuItem to='/contests'>Все конкурсы</MenuItem>
        {me
          ? (
            <Fragment>
              <MenuItem to={`/${me.slug}`}>Мой профиль</MenuItem>
              <Button view='ghost' to='/create'>Создать конкурс</Button>
            </Fragment>
          )
          : <Button view='ghost' to='/login'>Войти</Button>
        }
      </MenuContent>
    </Box>
  </Flex>

Menu.propTypes = {
  isMobile: PropTypes.bool.isRequired,
  show: PropTypes.bool.isRequired,
  toggleMenu: PropTypes.func.isRequired,
  me: PropTypes.object
}

const enhance = compose(
  withStateHandlers(
    {
      show: false
    },
    {
      toggleMenu: ({ show }) => () => ({ show: !show })
    }
  ),
  withHandlers({
    handleClickOutside: ({ show, toggleMenu }) => () => show && toggleMenu()
  }),
  withSizes(({ width }) => ({
    isMobile: width < MOBILE_WIDTH,
    disableOnClickOutside: width > MOBILE_WIDTH
  })),
  onClickOutside
)

export default enhance(Menu)
