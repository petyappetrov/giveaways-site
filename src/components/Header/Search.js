/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { isEmpty, map } from 'lodash'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { Query } from 'react-apollo'
import { withRouter } from 'react-router-dom'
import { compose, withStateHandlers, withHandlers, withProps } from 'recompose'
import onClickOutside from 'react-onclickoutside'
import SearchQuery from 'schemas/SearchQuery.gql'
import { Input, Button, Spinner } from 'components'

const SearchLayout = styled.div`
  position: relative;
`

const SearchResultsWrapper = styled.div`
  position: absolute;
  width: 100%;
  top: 100%;
  margin-top: 8px;
  box-sizing: border-box;
  left: 0;
  padding: 16px;
  border-radius: 10px;
  box-shadow: ${p => p.theme.shadow};
  background: #fff;
`

const SearchInput = styled(Input)`
  border: none;
  width: 320px;
  box-shadow: ${p => p.theme.shadow};
`

const SearchResultItem = styled.div`
  padding: 10px;
  text-align: left;
  display: flex;
  justify-content: space-between;
`

const SearchResultsList = ({ data, loading, push }) => {
  if (loading) {
    return <Spinner />
  }
  if (
    isEmpty(data.users.items) &&
    isEmpty(data.contests.items)
  ) {
    return <span>По вашему запросу ничего не найдено</span>
  }
  return (
    <Fragment>
      <Fragment>
        {
          map(data.users.items, (item) =>
            <SearchResultItem key={item._id}>
              <a onClick={() => push(`/${item.slug}`)}>{item.name}</a>
              <span>Пользователь</span>
            </SearchResultItem>
          )
        }
      </Fragment>
      <Fragment>
        {
          map(data.contests.items, (item) =>
            <SearchResultItem key={item._id}>
              <a onClick={() => push(`/${item.slug}`)}>{item.title}</a>
              <span>Конкурс</span>
            </SearchResultItem>
          )
        }
      </Fragment>
    </Fragment>
  )
}

SearchResultsList.propTypes = {
  data: PropTypes.object.isRequired,
  push: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
}

const Search = ({ value, setValue, visible, setVisible, push }) =>
  <Query
    skip={!value}
    query={SearchQuery}
    variables={{ search: value }}
    notifyOnNetworkStatusChange
  >
    {({ data, loading, refetch }) =>
      <SearchLayout>
        <SearchInput
          value={value}
          onFocus={() => setVisible(true)}
          onChange={(event) => setValue(event.target.value)}
          placeholder='Поиск конкурса или пользователя'
        />
        <Button
          ml={1}
          view='primary'
          onClick={() => refetch({ search: value }).then(() => !visible && setVisible(true))}
          disabled={loading && !!value}
        >
          <FontAwesomeIcon icon={faSearch} />
        </Button>
        {visible && value &&
          <SearchResultsWrapper>
            <SearchResultsList
              data={data}
              push={push}
              loading={loading}
              setVisible={setVisible}
            />
          </SearchResultsWrapper>
        }
      </SearchLayout>
    }
  </Query>

Search.propTypes = {
  value: PropTypes.string.isRequired,
  setValue: PropTypes.func.isRequired,
  visible: PropTypes.bool.isRequired,
  setVisible: PropTypes.func.isRequired,
  push: PropTypes.func.isRequired
}

const enhance = compose(
  withRouter,
  withStateHandlers(
    {
      value: '',
      visible: false
    },
    {
      setValue: () => (value) => ({ value }),
      setVisible: () => (visible) => ({ visible })
    }
  ),
  withHandlers({
    handleClickOutside: ({ setVisible }) => () => setVisible(false),
    push: ({ history, setVisible }) => (to) => {
      setVisible(false)
      history.push(to)
    }
  }),
  withProps(({ visible }) => ({ disableOnClickOutside: !visible })),
  onClickOutside
)

export default enhance(Search)
