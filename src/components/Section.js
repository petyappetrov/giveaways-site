/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'
import { space } from 'styled-system'
import tag from 'clean-tag'

const Section = styled(tag).attrs({
  p: p => p.p || [2, 3]
})`
  ${space}
  background: #fff;
  border-radius: 10px;
  box-sizing: border-box;
  overflow: ${p => p.overflow || 'visible'};
  box-shadow: ${p => p.theme.shadow};
  ${p => p.fullHeight && 'height: 100%;'}
`

Section.Title = styled(tag).attrs({
  mb: p => p.mb || 15
})`
  ${space}
  font-weight: 700;
  font-size: 24px;
`

export default Section
