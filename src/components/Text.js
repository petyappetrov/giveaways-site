/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'
import { space, fontSize, fontWeight, textAlign, lineHeight, width } from 'styled-system'

const Title = styled.div`
  line-height: 1.618em;
  ${p => p.error && 'color: #CD0F21'}
  ${p => p.color ? `color: ${p.color}` : ''};
  ${width}
  ${space}
  ${fontSize}
  ${fontWeight}
  ${textAlign}
  ${lineHeight}
`

export default Title
