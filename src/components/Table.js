/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { map, isFunction } from 'lodash'
import { withSizes } from 'react-sizes'
import { Box, Flex } from 'components'

const TableWrapper = styled.table`
  width: 100%;
`

const TableHeader = styled.thead`
  text-align: left;
`

const TableHeaderColumn = styled.th`
  padding-bottom: 0px;
  vertial-align: bottom;
`

const TableBodyColumn = styled.td`
  padding: 15px 10px 0 0;
  &:last-child {
    padding-right: 0;
  }
`

const DesktopTable = ({ data, columns }) =>
  <TableWrapper>
    <TableHeader>
      <tr>
        {map(columns, (column, i) =>
          <TableHeaderColumn key={i}>{column.header}</TableHeaderColumn>
        )}
      </tr>
    </TableHeader>
    <tbody>
      {map(data, (contest, i) =>
        <tr key={i}>
          {map(columns, (column, i) =>
            <TableBodyColumn key={i}>
              {isFunction(column.transform)
                ? column.transform(contest[column.accessor], contest)
                : contest[column.accessor]
              }
            </TableBodyColumn>
          )}
        </tr>
      )}
    </tbody>
  </TableWrapper>

DesktopTable.propTypes = {
  data: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired
}

const MobileTable = ({ data, columns }) =>
  <Flex flexDirection='column'>
    {map(data, (contest, i) =>
      <Box key={i} my={15}>
        {map(columns, (column, i) =>
          <Flex key={i} mb={5} justify='space-between'>
            <Box><strong>{column.header}:</strong></Box>
            <Box>
              {isFunction(column.transform)
                ? column.transform(contest[column.accessor], contest)
                : contest[column.accessor]
              }
            </Box>
          </Flex>
        )}
      </Box>
    )}
  </Flex>

MobileTable.propTypes = {
  data: PropTypes.array.isRequired,
  columns: PropTypes.array.isRequired
}

const Table = ({ isMobile, ...props }) => {
  if (isMobile) {
    return <MobileTable {...props} />
  }
  return <DesktopTable {...props} />
}

Table.propTypes = {
  isMobile: PropTypes.bool.isRequired
}

const enhance = withSizes(({ width }) => ({ isMobile: width < 768 }))

export default enhance(Table)
