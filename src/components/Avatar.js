/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/free-solid-svg-icons'

const AvatarWrapper = styled.div`
  width: 160px;
  height: 160px;
  border-radius: 50%;
  overflow: hidden;
  background: #fff;
  margin-top: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: ${p => p.theme.shadow};
  > img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`

const Avatar = ({ src }) =>
  <AvatarWrapper>
    {src
      ? <img src={src} />
      : <FontAwesomeIcon icon={faUser} size='5x' />

    }
  </AvatarWrapper>

Avatar.propTypes = {
  src: PropTypes.string
}

export default Avatar
