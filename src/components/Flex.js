/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'
import tag from 'clean-tag'
import {
  space,
  flexWrap,
  width,
  flexDirection,
  alignItems,
  textAlign,
  justifyContent,
  flex
} from 'styled-system'

const Flex = styled(tag).attrs({
  mx: p => p.mx || -2
})`
  display: flex;
  flex-wrap: wrap;
  ${space}
  ${width}
  ${flexWrap}
  ${flexDirection}
  ${alignItems}
  ${textAlign}
  ${justifyContent}
  ${flex}
`

export default Flex
