/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import onClickOutside from 'react-onclickoutside'
import { space } from 'styled-system'

const activeCSS = `
  background: #fff;
  color: black;
  &::after {
    border-top-color: black;
    border-bottom-color: black;
  }
`

const SelectButton = styled.div`
  background: #fff;
  border: 1px solid ${p => p.theme.colors.blue};
  box-sizing: border-box;
  border-radius: 10px;
  line-height: 38px;
  height: 40px;
  padding: 0 20px;
  font-weight: 900;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: space-between;
  ::after {
    content: '';
    margin-left: 8px;
    width: 0;
    height: 0;
    border-left: 6px solid transparent;
    border-right: 6px solid transparent;
    border-top: 6px solid ${p => p.theme.colors.blue};
    ${p => p.active && `
      border-top: none;
      border-left: 6px solid transparent;
      border-right: 6px solid transparent;
      border-bottom: 6px solid white;
    `}
  }
  &:hover {
    ${activeCSS}
  }
  ${p => p.active && activeCSS}
`

const SelectWrapper = styled.div`
  ${space}
  position: relative;
  min-width: 240px;
  display: inline-block;
`

const SelectList = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  background: #fff;
  border-radius: 10px;
  padding: 20px;
  z-index: 2;
  text-align: left;
  box-shadow: ${p => p.theme.shadow};
`

const SelectOption = styled.div`
  padding: 10px;
  font-weight: 900;
  cursor: pointer;
`

class Select extends React.Component {
  static propTypes = {
    options: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    label: PropTypes.string,
    value: PropTypes.string
  }

  state = {
    isOpen: false
  }

  toggleList = () => {
    this.setState({ isOpen: !this.state.isOpen })
  }

  onChange = (value) => {
    this.setState({ isOpen: false }, () =>
      this.props.onChange(value)
    )
  }

  handleClickOutside = (e) => {
    if (this.state.isOpen) {
      this.setState({
        isOpen: false
      })
    }
  }

  getSelectedLabel = () => {
    const selectedOption = this.props.options.find(option =>
      option.value && option.value.toString() === this.props.value
    )
    if (!selectedOption) {
      return this.props.label || 'Выберите'
    }
    return selectedOption.label
  }

  render () {
    const { options, ...props } = this.props
    if (!options.length) {
      return null
    }
    return (
      <SelectWrapper {...props}>
        <SelectButton onClick={this.toggleList} active={this.state.isOpen}>
          {this.getSelectedLabel()}
        </SelectButton>
        {this.state.isOpen &&
          <SelectList>
            {options.map(({ label, value }, i) =>
              <SelectOption
                key={i}
                value={value}
                onClick={() => this.onChange(value)}
              >
                {label}
              </SelectOption>
            )}
          </SelectList>
        }
      </SelectWrapper>
    )
  }
}

export default onClickOutside(Select)
