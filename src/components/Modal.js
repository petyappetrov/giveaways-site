/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import onClickOutside from 'react-onclickoutside'
import { withHandlers, compose } from 'recompose'

const Overlay = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  justify-content: center;
  background-color: rgba(255, 255, 255, .9);
  cursor: pointer;
  z-index: 11;
`

const Block = styled.div`
  box-shadow: ${p => p.theme.shadow};
  margin-top: 160px;
  margin-bottom: auto;
  flex: 0 1 auto;
  width: 640px;
  border-radius: 10px;
  padding: 32px;
  background-color: #fff;
  cursor: default;
`

const InnerBlock = compose(
  withHandlers(() => ({
    handleClickOutside: ({ closeModal }) => closeModal
  })),
  onClickOutside
)(Block)

class Modal extends React.PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired,
    isOpen: PropTypes.bool.isRequired,
    closeModal: PropTypes.func.isRequired
  }

  element = document.createElement('div')

  componentDidMount () {
    document.getElementById('root').appendChild(this.element)
    document.addEventListener('keydown', this.handleKeydown)
  }

  componentWillUnmount () {
    document.getElementById('root').removeChild(this.element)
    document.removeEventListener('keydown', this.handleKeydown)
  }

  handleKeydown = (event) => {
    const { isOpen, closeModal } = this.props
    if (event.keyCode === 27 && isOpen && typeof closeModal === 'function') {
      closeModal()
    }
  }

  render () {
    if (!this.props.isOpen) {
      return null
    }
    return ReactDOM.createPortal(
      <Overlay>
        <InnerBlock closeModal={this.props.closeModal}>
          {this.props.children}
        </InnerBlock>
      </Overlay>,
      this.element
    )
  }
}

export default Modal
