/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

export const InputStyled = styled.input`
  height: 40px;
  font-size: 1rem;
  box-sizing: border-box;
  border: 1px solid #ddd;
  padding-left: 15px;
  padding-right: 15px;
  background-color: #fff;
  border-radius: 10px;
  width: ${p => p.fullWidth ? '100%' : 'auto'};
  ${(p) => p.error && 'border-color: #CD0F21;'}
`

const Input = ({
  name,
  ...props
}) =>
  <InputStyled
    name={name}
    id={name}
    {...props}
  />

Input.defaultProps = {
  type: 'text',
  value: ''
}

Input.propTypes = {
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func,
  value: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string
}

export default Input
