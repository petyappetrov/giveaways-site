/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'

const Container = styled.div`
  margin: auto;
  max-width: 1280px;
  margin: 0 auto;
  padding: 0 2rem;
  flex-grow: 1;
  position: relative;
  @media (max-width: 32rem) {
    padding: 0 1rem;
  }
`
export default Container
