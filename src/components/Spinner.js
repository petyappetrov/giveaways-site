/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'
import { Flex } from 'components'

export default () =>
  <Flex alignItems='center' justifyContent='center' flex='1 1 auto'>
    <FontAwesomeIcon icon={faSpinner} spin />
  </Flex>
