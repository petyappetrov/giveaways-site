/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'
import { find } from 'lodash'
import gradients from './gradients'

const Gradient = styled.div`
  width: 100%;
  height: ${p => p.height || 360}px;
  background: ${p => {
    const { colors } = find(gradients, { name: p.name })
    return `linear-gradient(to right, ${colors[0]}, ${colors[1]})`
  }};
`

Gradient.defaultProps = {
  name: 'Sweet Dream'
}

export default Gradient
