/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import { injectGlobal } from 'styled-components'
import styledNormalize from 'styled-normalize'

export const theme = {
  space: [0, 8, 16, 32, 48, 60],
  shadow: '0 5px 40px rgba(29, 34, 50, .06), 0 10px 90px 4px rgba(29, 34, 50, .08)',
  colors: {
    blue: '#7F7AF9',
    black: '#343E5F'
  }
}

export default injectGlobal`
  ${styledNormalize}
  html,
  body,
  #root {
    width: 100%;
    height: 100%;
    position: relative;
    color: ${theme.colors.black};
  }
  #root {
    display: flex;
    min-height: 100vh;
    flex-direction: column;
  }
  *,
  button,
  input,
  optgroup,
  select,
  textarea {
    font-family: 'PT Sans', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  input[type='text'],
  input[type='password'],
  textarea,
  button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    &:focus {
      border-color: #000;
      box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 0 3px rgba(0, 0, 0, 0.1);
      outline: none;
    }
  }
  textarea::placeholder,
  input[type='password']::placeholder,
  input[type='text']::placeholder {
    color: #aaa;
  }
  a {
    color: ${theme.colors.blue};
    font-weight: bold;
    text-decoration: none;
    &:hover {
      text-decoration: underline;
    }
  }
`
