/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

const Joi = require('joi')

/**
 * Parse .env file and include in process.env
 */
require('dotenv').config()

/**
 * Schema for env variables
 */
const envSchema = Joi.object({
  NODE_ENV: Joi.string().required(),
  GRAPHQL_URI: Joi.string().required(),
  PORT: Joi.number().required()
})

/**
 * Validate env variables
 */
const { error, value } = Joi.validate(process.env, envSchema.unknown())
if (error) {
  console.error(error.message)
}

/**
 * Project configuration variables
 */
module.exports = {
  env: value.NODE_ENV,
  graphqlURI: value.GRAPHQL_URI,
  port: value.PORT
}
